from flask import Flask, session, abort
import uuid
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor
from views import *
import utils

app = Flask(__name__)

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'aassdd'
app.config['MYSQL_DATABASE_DB'] = 'robotics'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['MYSQL_CURSOR_CLASS'] = 'DictCursor'
app.config['CRYPTOHUB_KEY'] = 'NoneuhoqSRsVzJMBhKrA'
app.secret_key = 'fgsjfhsghsgh775'

mysql = MySQL(cursorclass=DictCursor)
mysql.init_app(app)
app.conn = mysql.connect()
app.settings = utils.faucet_settings(app.conn)


@app.route("/")
def index():
    return index_view(app)


@app.route('/claim/', methods=['POST'])
def claim():
    return claim_view(app)


@app.context_processor
def utility_processor():
    def format_price(amount,precise=8):
        try:
            return ('{0:.'+str(precise)+'f}').format(amount)
        except:
            return "0"
    return dict(format_price=format_price)


@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.get('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)

def generate_csrf_token():
    if '_csrf_token' not in session:
        print("gen new token")
        session['_csrf_token'] = str(uuid.uuid4().hex.upper()[0:6])
    return session['_csrf_token']

app.jinja_env.globals['csrf_token'] = generate_csrf_token


if __name__ == '__main__':
    app.run(debug=True)