function htmlspecialchars(str) {
    var map = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        "\"": "&quot;",
        "'": "&#39;" // ' -> &apos; for XML only
    };
    return str.replace(/[&<>"']/g, function(m) { return map[m]; });
}
function htmlspecialchars_decode(str) {
    var map = {
        "&amp;": "&",
        "&lt;": "<",
        "&gt;": ">",
        "&quot;": "\"",
        "&#39;": "'"
    };
    return str.replace(/(&amp;|&lt;|&gt;|&quot;|&#39;)/g, function(m) { return map[m]; });
}

$(document).ready(function(){
    $(".robotcaptcha div img").click(function(){
        div = $(this).closest("div");
        if(div.hasClass("sel")){
            div.removeClass("sel");
            div.find("input").prop("checked",false);
        }else{
            div.addClass("sel");
            div.find("input").prop("checked",true);
        }
    });

    $("main .claimform .subm").click(function(){
        var url = $(this).closest("form").attr("action");
        var data = $(this).closest("form").serialize();
        $(".claimform table").slideUp(300, function(){
            $(".claimform .claiming").fadeIn(500,function(){
                $.post(url,data,function(ret){
                    console.log(ret);
                    if(ret.error){
                        $(".claimform .claiming").addClass("error");
                        $(".claimform .claiming .ret").html("Error: " + String(ret.error));
                        $(".claimform .claiming .reload").show();
                    }else if(ret.success){
                        $(".claimform .claiming").addClass("success");
                        $(".claimform .claiming .ret").html(ret.message);
                    }
                });
            });
        });
        return false;
    });

    $("main .claiming .reload").click(function(){
        location.reload();
    });





    function chat_smiles_replace(msg)
    {
        return msg;
    }



    //chat
    if($(".chat_block").length){

        var socket = new WebSocket("ws://xlr-faucet.club:8002/");
        //var socket = new WebSocket("ws://xlr-faucet.club:8002/");

        nick = $(".chat_form .nick").text();

        socket.onopen = function() {

          $(".chat_form .btn").click(function(){
              txt = encodeURI($(this).closest(".chat_form").find(".inp").val());
              if(txt.length){
                  socket.send(JSON.stringify({"txt":txt}));
                  nick = htmlspecialchars(decodeURI(nick));
                  msg = chat_smiles_replace(htmlspecialchars(decodeURI(txt)));
                  tpl = '<div><label>'+nick+'</label>'+msg+'</div>';
                  $(".chat_block").find(".chat_list .chat_lng").append(tpl);
              }
              $(this).closest(".chat_form").find(".inp").val("");
              var top = $(".chat_list").find(".chat_lng")[0].scrollHeight;
              $(".chat_list").find(".chat_lng").scrollTop(top);
              return false;
          });


          $(".chat_form .inp").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    $(".chat_form input.btn").click();
                }
            });

        };

        socket.onclose = function(event) {
          if (event.wasClean) {
            //alert('Соединение закрыто чисто');
          } else {
            //alert('Обрыв соединения'); // например, "убит" процесс сервера
          }
          $(".chat_form").html("can't connect to chat socket");
          //alert('Код: ' + event.code + ' причина: ' + event.reason);
        };

        socket.onmessage = function(event) {
          data = JSON.parse(event.data);
          if(data.online || data.online === 0){
            $(".chat_block .title span i").html(data.online);
          }
          if(data.txt){
            msg = chat_smiles_replace(htmlspecialchars(decodeURI(data.txt[1])));
            nick = htmlspecialchars(decodeURI(data.txt[0]));
            msg = msg;
            msg = msg.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '[url deleted]');
            tpl = '<div><label>'+nick+'</label>'+msg+'</div>';
            $(".chat_block").find(".chat_list .chat_lng").append(tpl);
            var top = $(".chat_list").find(".chat_lng")[0].scrollHeight;
            $(".chat_list").find(".chat_lng").scrollTop(top);
          }
          if(data.ban){
             $(".chat_form").html("You are banned in the chat until " + String(data.ban_until));
          }
        };

        socket.onerror = function(error) {
           //alert("Ошибка " + error.message);
        };


        $("html").on("click",".chat_msgs div label",function(){
            txt = $(this).text();
            if($(".chat_form .inp").length){
                $(".chat_form .inp").val("@" + txt + " ");
            }
            return false;
        });



    }







});