import signal
import sys
import ssl
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer, SimpleSSLWebSocketServer
from optparse import OptionParser
import json
import MySQLdb
import datetime

#db = MySQLdb.connect(host="localhost", user="root", passwd="solar25758", db="robotics")
db = MySQLdb.connect(host="localhost", user="root", passwd="aassdd", db="robotics")

clients = []

last_msgs = {
    "en": [],
    "es": [],
    "ru": []
}

def chat_log(txt):
    with open("chat_log.txt", "a") as myfile:
        myfile.write(str(txt) + "\n")


class SimpleChat(WebSocket):

    def handleMessage(self):
        try:
            ip = self.address[0]
            data = json.loads(self.data)

            lng = "en"
            if lng in ["en","es","ru"]:
                nick = None
                blocked = False

                cur = db.cursor()
                cur.execute("SELECT id FROM chat_blocked WHERE ip=%s" , [ip])
                res = cur.fetchone()
                if res:
                    if res[0]:
                        blocked = True


                if not blocked:
                    cur.execute("SELECT id,nick FROM chat_nick WHERE ip=%s" , [ip])
                    res = cur.fetchone()
                    if res:
                        if res[1]:
                            nick = res[1]


                if blocked:
                    self.sendMessage(json.dumps({"ban":True}))

                if len(data["txt"]) > 0 and not blocked:
                    if not nick:
                        nick = ip.split(".")[0] + "..."
                    msg = [nick,data["txt"]]
                    last_msgs[lng].append(msg)
                    if len(last_msgs[lng]) > 30:
                        last_msgs[lng] = last_msgs[lng][-30:]
                    for client in clients:
                        if client != self:
                            client.sendMessage(json.dumps({"txt":msg}))
        except Exception as e:
            print("err")
            print(e)
            chat_log(["handleMessage", e])


    def handleConnected(self):
       print(self.address, 'connected')
       for lang,msgs in last_msgs.items():
           for msg in msgs:
               self.sendMessage(json.dumps({"txt": msg}))
       clients.append(self)
       for client in clients:
          client.sendMessage(json.dumps({"online":len(clients)}))


    def handleClose(self):
       clients.remove(self)
       print(self.address, 'closed')
       for client in clients:
           client.sendMessage(json.dumps({"online": len(clients)}))

if __name__ == "__main__":

   parser = OptionParser(usage="usage: %prog [options]", version="%prog 1.0")
   parser.add_option("--host", default='', type='string', action="store", dest="host", help="hostname (localhost)")
   parser.add_option("--port", default=8001, type='int', action="store", dest="port", help="port (8000)")
   parser.add_option("--example", default='echo', type='string', action="store", dest="example", help="echo, chat")
   parser.add_option("--ssl", default=0, type='int', action="store", dest="ssl", help="ssl (1: on, 0: off (default))")
   parser.add_option("--cert", default='./cert.pem', type='string', action="store", dest="cert", help="cert (./cert.pem)")
   parser.add_option("--key", default='./key.pem', type='string', action="store", dest="key", help="key (./key.pem)")
   parser.add_option("--ver", default=ssl.PROTOCOL_TLSv1, type=int, action="store", dest="ver", help="ssl version")

   (options, args) = parser.parse_args()

   cls = SimpleChat

   if options.ssl == 1:
      server = SimpleSSLWebSocketServer(options.host, options.port, cls, options.cert, options.key, version=options.ver)
   else:
      server = SimpleWebSocketServer(options.host, options.port, cls)

   def close_sig_handler(signal, frame):
      server.close()
      sys.exit()

   signal.signal(signal.SIGINT, close_sig_handler)

   server.serveforever()
