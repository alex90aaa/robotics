import os, random, copy
from PIL import Image, ImageEnhance
import base64

robots = {
    "static/images/robot1.png": [[103,27],[10,111]],
    "static/images/robot2.png": [[20,10]],
    "static/images/robot3.png": [[84,31]]
}

coins_dir = "static/images/coins"
cache_dir = "static/images/cache"


def gen_task():
    coins = {}
    dir = os.listdir(coins_dir)
    for el in dir:
        if "(" in el:
            key = el.split("(")[0]
        else:
            key = el.split(".")[0]
        if not key in coins:
            coins[key] = []
        coins[key].append(el)
    right_coin_i = random.randint(1,len(coins))
    right_coin = None
    other_coins = {}
    i=0
    for el in coins:
        i+=1
        if i == right_coin_i:
            right_coin = el
        else:
            other_coins[el] = el
    print(coins)
    print("right_coin", right_coin_i,  right_coin, other_coins)
    rights = 0
    rights_pos = []
    imgs=[]
    for i in range(6):
        if ((random.randint(1,3) == 2) and rights < 3) or (i > 3 and rights < 1):
            img = gen_img(right_coin, coins)
            rights += 1
            rights_pos.append(i+1)
        else:
            coin = random.choice(list(other_coins.keys()))
            img = gen_img(coin, coins)
        imgs.append(img.decode("utf-8"))
        print("rights", rights)
    return [right_coin,rights_pos,imgs]


def gen_img(coin, coins):
    rnd = random.choice(list(robots.keys()))
    rnd_coin_size = random.choice([30,32,35])
    robot = robots[rnd]
    coords = random.choice(robot)
    coin_img_rand = random.choice(coins[coin])
    enhance_level = random.randint(1, 6)
    rotate = random.choice([10,20,30,40,50,80,100,120,70])

    #check cache
    cache_code = str(coords[0]) + "_" + str(coords[1])  + "_" + str(coin_img_rand) + "_" + str(enhance_level) + "_" + str(rotate) + ".png"
    cache_file = cache_dir + "/" + cache_code
    if os.path.isfile(cache_file):
        print("cache found")
        with open(cache_file, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
            return encoded_string
    else:
        print("no cache")


    img = Image.open(rnd)
    coin_img = Image.open(coins_dir + "/" + coin_img_rand)
    coin_img = coin_img.resize((rnd_coin_size,rnd_coin_size), Image.ANTIALIAS).rotate(rotate, resample=Image.BILINEAR)
    contrast = ImageEnhance.Contrast(coin_img)
    coin_img = contrast.enhance(enhance_level)
    final2 = Image.new("RGBA", img.size)
    final2 = Image.alpha_composite(final2, img)
    final3 = Image.new("RGBA", img.size)
    final3.paste(coin_img, [coords[0],coords[1]])
    final2 = Image.alpha_composite(final2, final3)
    final2.save(cache_file)

    with open(cache_file, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
        return encoded_string




if __name__ == '__main__':
    gen_task()