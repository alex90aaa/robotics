import datetime, json, decimal, random
from urllib.request import urlopen

def escape(text):
    return text.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")

def upd_tbl(cur,tbl,data,where=None):
    arg = []
    query = "UPDATE " + str(tbl) + " SET "
    for k,v in data.items():
        if len(arg):
            query += ", "
        query += k + "=%s"
        arg.append(v)
    if where:
        wheres = ""
        for k, v in where.items():
            if len(wheres):
                wheres += " AND "
            wheres += k + "=%s"
            arg.append(v)
    cur.execute(query,arg)


nick_words = ["sun","sunstrike","solar","sunny","solaris","sunflower","sunlight","sunrise","sol","sunburst"]
def gen_nick():
    word = random.choice(nick_words)
    number = random.randint(1,100)
    return word + str(number)


def get_nick(conn, ip):
    cur = conn.cursor()
    cur.execute("SELECT * FROM chat_nick WHERE ip=%s",[ip])
    row = cur.fetchone()
    if row:
        return row["nick"]
    else:
        nick = gen_nick()
        cur.execute("INSERT INTO chat_nick (id,ip,nick) VALUES (0,%s,%s)",[ip,nick])
        return nick




def faucet_settings(conn, settings=None):
    cur = conn.cursor()
    if settings:
        upd_tbl(cur, "faucet", settings)
    cur.execute("SELECT * FROM faucet")
    row = cur.fetchone()
    print(row)
    if row:
        return row
    else:
        cur.execute("INSERT INTO faucet (id) VALUES (0)")
        print("here")


def http_req(url,post=None):
    try:
        r = urlopen(url,post)
        rr = json.loads(r.read().decode())
        return rr
    except Exception as e:
        return str(e)

def reward_amount(bal):
    if not bal:
        return 0
    reward = decimal.Decimal(bal / 1000)
    reward += decimal.Decimal(0.000005)
    reward = round(reward,5)
    if reward < 0.00001:
        reward = 0.00001
    return reward

#return seconds remaining
def check_time_remain(conn, ip, adr=None):
    cur = conn.cursor()
    query = "SELECT * FROM claims WHERE ip=%s"
    args = [ip]
    if adr:
        query += " AND adr=%s"
        args.append(adr)
    query += " ORDER BY id DESC"
    cur.execute(query,args)
    row = cur.fetchone()
    if not row:
        return 0
    else:
        dif = (datetime.datetime.now() - row["dt"]).seconds
        dif = 1800 - dif
        if dif < 0:
            return 0
        return dif



def pay_reward(conn, ip, adr, amount):
    res = http_req("https://cryptohub.online/api/faucets/payout/?key=NoneP7TI67C5Dp8XKJs7&adress="+str(adr)+"&amount="+str(amount))
    if "error" in res:
        return {"error": res["error_text"]}
    else:
        cur = conn.cursor()
        query = "INSERT INTO claims (ip,amount,dt,adr) VALUES (%s,%s,%s,%s)"
        args = [ip, amount, datetime.datetime.now(), adr]
        cur.execute(query, args)
        return res


def get_bal(app):
    if not app.settings["balance_updated"] or app.settings["balance_updated"] < datetime.datetime.now() - datetime.timedelta(minutes=2):
        #make request
        bal = http_req("https://cryptohub.online/api/faucets/balance/?key=NoneP7TI67C5Dp8XKJs7")
        if 'balance' in bal:
            app.settings["balance"] = bal['balance']
            app.settings["balance_updated"] = datetime.datetime.now()
        faucet_settings(app.conn, app.settings)
        return app.settings["balance"]
    else:
        return app.settings["balance"]