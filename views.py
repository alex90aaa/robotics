from flask import Flask, session, render_template, request, jsonify
import utils
import json
from captcha_gen import gen_task

def index_view(app):
    bal = utils.get_bal(app)
    print(bal)
    reward = utils.reward_amount(bal)
    task = gen_task()
    session['task_correct'] = task[1]
    if 'adr' in session:
        adr = session['adr']
    else:
        adr = ""
    need_wait = utils.check_time_remain(app.conn, request.remote_addr, adr)

    nick = utils.get_nick(app.conn,request.remote_addr)

    return render_template("index.html", **{"balance":bal,"reward":reward,"task":task, "adr":adr, "need_wait":need_wait, "nick":nick})

def claim_view(app):
    bal = utils.get_bal(app)
    reward = utils.reward_amount(bal)
    print(bal)

    adr = utils.escape(request.form.get('adr'))
    # check adr format
    if not (len(adr) == len("SVXiaooFKYqRL83MTvvmw7cRt7yG8FkDYJ") and adr[0] == "S"):
        return jsonify({"error":"Wrong Solaris address"})

    session['adr'] = adr

    print("here", request.form)
    selected_robot = request.form.getlist('selected_robot')
    print(session['task_correct'], selected_robot)

    need_wait = utils.check_time_remain(app.conn, request.remote_addr, adr)
    if need_wait > 0:
        return jsonify({"error": "You need wait " + str() + " seconds"})

    if 'task_correct' in session:
        if len(session['task_correct']) >= 1:
            correct = 0
            for el in session['task_correct']:
                if str(el) in selected_robot:
                    correct += 1
            if correct == len(session['task_correct']) and len(selected_robot) == len(session['task_correct']):
                #correct value
                session['task_correct'] = []
                ret = utils.pay_reward(app.conn, request.remote_addr, adr, reward)
                return jsonify(ret)

    session['task_correct'] = []

    return jsonify({"error":"Wrong captcha"})