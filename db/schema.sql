create database if not exists robotics CHARACTER SET utf8 COLLATE utf8_general_ci;
use robotics;
drop table if exists faucet;
create table faucet (
  id integer primary key auto_increment,
  balance decimal(14,9) null,
  balance_updated datetime null,
  chat_online_cnt int(11) null,
  chat_online_cnt_updated datetime null
);

drop table if exists chat_nick;
create table chat_nick (
  id integer primary key auto_increment,
  ip text not null,
  nick text not null
);

drop table if exists chat_blocked;
create table chat_blocked (
  id integer primary key auto_increment,
  ip text not null
);

drop table if exists claims;
create table claims (
  id integer primary key auto_increment,
  ip text not null,
  amount decimal(14,9) null,
  dt datetime null,
  adr text null
);